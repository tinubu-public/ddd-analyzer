/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.diagram.plantuml;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tsquare.dddanalyzer.core.diagram.plantuml.PlantUmlRepresentation.Creole.DEFAULT;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Comparator.comparing;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.criterion.Criterion;
import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.type.BiCompositeId;
import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.domain.type.TriCompositeId;
import com.tinubu.commons.ddd2.domain.type.Version;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.OpenDocumentMetadata.OpenDocumentMetadataBuilder;
import com.tsquare.dddanalyzer.core.diagram.DddRepresentation;
import com.tsquare.dddanalyzer.core.model.ArrayModel;
import com.tsquare.dddanalyzer.core.model.ContainerModel;
import com.tsquare.dddanalyzer.core.model.DomainObjectModel;
import com.tsquare.dddanalyzer.core.model.EntityModel;
import com.tsquare.dddanalyzer.core.model.FieldModel;
import com.tsquare.dddanalyzer.core.model.ObjectModel;
import com.tsquare.dddanalyzer.core.model.RawType;
import com.tsquare.dddanalyzer.core.model.RootEntityModel;
import com.tsquare.dddanalyzer.core.model.ValueModel;

public class PlantUmlRepresentation implements DddRepresentation {
   private final PlantUmlRepresentationConfig config;

   public PlantUmlRepresentation(PlantUmlRepresentationConfig config) {
      this.config = config;
   }

   public PlantUmlRepresentation() {
      this(new PlantUmlRepresentationConfig());
   }

   @Override
   public void generate(List<DomainObjectModel> model,
                        Predicate<DomainObjectModel> filter,
                        boolean forceDirectReferences,
                        DocumentRepository output,
                        DocumentPath documentId) {
      Document document = output
            .openDocument(documentId,
                          true,
                          false,
                          new OpenDocumentMetadataBuilder()
                                .contentType(mimeType("application", "plantuml")).contentEncoding(UTF_8)
                                .build())
            .orElseThrow(() -> new IllegalStateException(String.format("Can't create '%s' document",
                                                                       documentId.stringValue())));
      try (PrintWriter puml = new PrintWriter(new BufferedWriter(document.content().writerContent()))) {
         generate(puml, model, filter, forceDirectReferences);
      }
   }

   private void startuml(PrintWriter puml) {
      puml.println("@startuml");
      this.config.theme().ifPresent(theme -> puml.printf("!theme %s%n", theme));
      puml.println("hide empty members");
      puml.println("hide stereotype");
      this.config.header().map(String::trim).ifPresent(puml::println);
      puml.println();
   }

   private String typeName(ObjectModel objectModel, boolean simple) {
      return typeName(objectModel.type(), simple);
   }

   private String typeName(RawType<?> type, boolean simple) {
      return type.typeClass().map(c -> simple ? c.getSimpleName() : c.getName()).orElse(type.typeName());
   }

   private void generate(PrintWriter puml,
                         List<DomainObjectModel> model,
                         Predicate<DomainObjectModel> filter,
                         boolean forceDirectReferences) {
      startuml(puml);

      Map<RawType<? extends DomainObject>, DomainObjectModel> indexedModel =
            model.stream().collect(toMap(DomainObjectModel::type, identity()));

      Stream<DomainObjectModel> restrictedModel = model
            .stream()
            .filter(dom -> !ignoreCommonDomainObjects(dom))
            .filter(filter)
            .sorted(comparing(dom -> dom.type().typeName()));

      if (forceDirectReferences) {
         restrictedModel = restrictedModel
               .flatMap(dom -> streamConcat(stream(dom),
                                            stream(dom.references().values()).flatMap(reference -> stream(
                                                  nullable(indexedModel.get(reference.type()))))))
               .filter(dom -> !ignoreCommonDomainObjects(dom))
               .sorted(comparing(dom -> dom.type().typeName()));
      }

      LinkedHashMap<RawType<? extends DomainObject>, DomainObjectModel> indexedRestrictedModel =
            map(LinkedHashMap::new, restrictedModel.map(dom -> Pair.of(dom.type(), dom)));

      indexedRestrictedModel.values().forEach(object -> {

         /* TODO : does not work anymore in Plantuml :
            skinparam stereotypeC<<RootEntity>> {
              BackgroundColor #FF7700
            }
            skinparam spotChar<<RootEntity>> E
            skinparam stereotypeC<<Entity>> {
              BackgroundColor #FF7700
            }
            skinparam spotChar<<Entity>> E
            skinparam stereotypeC<<Value>> {
              Color #11AA11
            }
            skinparam spotChar<<Value>> V
          */

         String stereotype;
         if (object instanceof ValueModel) {
            stereotype = "<<(V," + config.valueCharacterColor() + ")Value>>";
         } else if (object instanceof RootEntityModel) {
            stereotype = "<<(E," + config.rootEntityCharacterColor() + ")RootEntity>>";
         } else if (object instanceof EntityModel) {
            stereotype = "<<(E," + config.entityCharacterColor() + ")Entity>>";
         } else {
            throw new IllegalStateException("Unknown domain object type : " + object.getClass().getName());
         }

         puml.println("class \""
                      + config.classNameCreole().creole(typeName(object, true))
                      + "\" as "
                      + typeName(object, false)
                      + " "
                      + stereotype
                      + " {");

         object.fields().stream().filter(isIdField(object)).forEach(f -> {
            puml.println(config.idPrefix().map(c -> c + " ").orElse("") + umlField(f));
            puml.println("..");
         });

         object
               .fields()
               .stream()
               .limit(config.maxFields())
               .filter(isIdField(object).negate())
               .filter(f -> config.showReferenceFields() || !(object.references().containsKey(f)
                                                              && indexedRestrictedModel.containsKey(object
                                                                                                          .references()
                                                                                                          .get(f)
                                                                                                          .type())))
               .forEach(f -> {
                  puml.println(umlField(f));
               });
         if (object.fields().size() > config.maxFields()) {
            puml.println("...");
         }

         puml.println("}");
         puml.println();

      });

      indexedRestrictedModel.values().forEach(object -> {
         object.references().forEach((field, reference) -> {
            if (indexedRestrictedModel.containsKey(reference.type()))
               puml.println(typeName(object, false)
                            + " \"1\" -"
                            + (StringUtils.repeat("-",
                                                  config.linkLength()))
                            + "> \""
                            + (field.type().map(ft -> ft instanceof ContainerModel).orElse(false) ? "*" : "1")
                            + "\" "
                            + typeName(reference, false)
                            + " : "
                            + field.name());
         });
      });
      enduml(puml);
   }

   private boolean ignoreCommonDomainObjects(DomainObjectModel reference) {
      return reference.type().isInstanceOf(Id.class)
             || reference.type().isInstanceOf(BiCompositeId.class)
             || reference.type().isInstanceOf(TriCompositeId.class)
             || reference.type().isInstanceOf(Version.class)
             || reference.type().isInstanceOf(Criterion.class)
             || reference.type().isInstanceOf(DomainEvent.class)
             || reference.type().isInstanceOf(com.tinubu.commons.ddd2.domain.type.DomainEvent.class);
   }

   private Predicate<FieldModel> isIdField(DomainObjectModel domainObjectModel) {
      return f -> domainObjectModel instanceof EntityModel && ((EntityModel) domainObjectModel)
            .idField()
            .equals(f);
   }

   private void enduml(PrintWriter puml) {
      puml.println("@enduml");
   }

   private String umlField(FieldModel f) {
      Creole fieldTypeCreole = config.fieldTypeCreole();
      Creole fieldNameCreole = config.fieldNameCreole();

      return f.type().map(fieldType -> {

         if (fieldType instanceof ArrayModel) {
            return fieldTypeCreole.creole(typeName(((ArrayModel) fieldType).containedType(), true) + "[]")
                   + " "
                   + fieldNameCreole.creole(f.name());
         } else {
            return fieldTypeCreole.creole(typeName(fieldType, true)) + " " + fieldNameCreole.creole(f.name());
         }
      }).orElse(fieldNameCreole.creole(f.name()));
   }

   public enum Creole {
      DEFAULT(null), BOLD('*'), ITALIC('/'), MONOSPACED('"'), UNDERLINED('_'), WAVE_UNDERLINED('~');

      private final Character creoleCharacter;

      Creole(Character creoleCharacter) {
         this.creoleCharacter = creoleCharacter;
      }

      public Character creoleCharacter() {
         return creoleCharacter;
      }

      public String creole(String text) {
         if (creoleCharacter == null) {
            return text;
         } else {
            return "" + creoleCharacter + creoleCharacter + text + creoleCharacter + creoleCharacter;
         }
      }
   }

   public static class PlantUmlRepresentationConfig {

      private int linkLength = 0;
      private String header;
      private String theme;
      private boolean showReferenceFields = true;
      private String idPrefix = "[id]";
      private int maxFields = 30;
      private String valueCharacterColor = "#11AA11";
      private String entityCharacterColor = "#FF7700";
      private String rootEntityCharacterColor = "#FF7700";
      private Creole fieldTypeCreole = DEFAULT;
      private Creole fieldNameCreole = DEFAULT;
      private Creole classNameCreole = DEFAULT;

      public int linkLength() {
         return linkLength;
      }

      public PlantUmlRepresentationConfig linkLength(int linkLength) {
         this.linkLength = linkLength;
         return this;
      }

      public Optional<String> header() {
         return nullable(header).filter(h -> !h.isEmpty());
      }

      public PlantUmlRepresentationConfig header(String header) {
         this.header = header;
         return this;
      }

      public Optional<String> theme() {
         return nullable(theme).filter(t -> !t.isEmpty());
      }

      public PlantUmlRepresentationConfig theme(String theme) {
         this.theme = theme;
         return this;
      }

      public boolean showReferenceFields() {
         return showReferenceFields;
      }

      public PlantUmlRepresentationConfig showReferenceFields(boolean showReferenceFields) {
         this.showReferenceFields = showReferenceFields;
         return this;
      }

      public Optional<String> idPrefix() {
         return nullable(idPrefix).filter(c -> !c.isEmpty());
      }

      public PlantUmlRepresentationConfig idPrefix(String idPrefix) {
         this.idPrefix = idPrefix;
         return this;
      }

      public int maxFields() {
         return maxFields;
      }

      public PlantUmlRepresentationConfig maxFields(int maxFields) {
         this.maxFields = maxFields;
         return this;
      }

      public String valueCharacterColor() {
         return valueCharacterColor;
      }

      public PlantUmlRepresentationConfig valueCharacterColor(String valueCharacterColor) {
         this.valueCharacterColor = valueCharacterColor;
         return this;
      }

      public String entityCharacterColor() {
         return entityCharacterColor;
      }

      public PlantUmlRepresentationConfig entityCharacterColor(String entityCharacterColor) {
         this.entityCharacterColor = entityCharacterColor;
         return this;
      }

      public String rootEntityCharacterColor() {
         return rootEntityCharacterColor;
      }

      public PlantUmlRepresentationConfig rootEntityCharacterColor(String rootEntityCharacterColor) {
         this.rootEntityCharacterColor = rootEntityCharacterColor;
         return this;
      }

      public Creole fieldTypeCreole() {
         return fieldTypeCreole;
      }

      public PlantUmlRepresentationConfig fieldTypeCreole(Creole fieldTypeCreole) {
         this.fieldTypeCreole = fieldTypeCreole;
         return this;
      }

      public Creole fieldNameCreole() {
         return fieldNameCreole;
      }

      public PlantUmlRepresentationConfig fieldNameCreole(Creole fieldNameCreole) {
         this.fieldNameCreole = fieldNameCreole;
         return this;
      }

      public Creole classNameCreole() {
         return classNameCreole;
      }

      public PlantUmlRepresentationConfig classNameCreole(Creole classNameCreole) {
         this.classNameCreole = classNameCreole;
         return this;
      }
   }
}
