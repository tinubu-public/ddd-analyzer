/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.model;

import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Array model.
 */
public class ArrayModel implements ContainerModel {

   private final ObjectModel containedType;

   public ArrayModel(ObjectModel containedType) {
      this.containedType = notNull(containedType, "containedType");
   }

   @Override
   public RawType<?> type() {
      throw new UnsupportedOperationException("Array has no type");
   }

   @Override
   public ObjectModel containedType() {
      return containedType;
   }

   @Override
   public List<RawType<?>> generics() {
      return emptyList();
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      ArrayModel that = (ArrayModel) o;
      return Objects.equals(containedType, that.containedType);
   }

   @Override
   public int hashCode() {
      return Objects.hash(containedType);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", ArrayModel.class.getSimpleName() + "[", "]")
            .add("containedType=" + containedType)
            .toString();
   }

}
