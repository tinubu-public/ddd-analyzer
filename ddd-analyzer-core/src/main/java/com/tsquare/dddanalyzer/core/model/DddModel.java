/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.model;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.or;
import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.type.BiCompositeId;
import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.domain.type.RootEntity;
import com.tinubu.commons.ddd2.domain.type.TriCompositeId;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.domain.type.Version;
import com.tinubu.commons.lang.beans.Getter;

import io.github.classgraph.ArrayTypeSignature;
import io.github.classgraph.BaseTypeSignature;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ClassInfoList.ClassInfoFilter;
import io.github.classgraph.ClassRefTypeSignature;
import io.github.classgraph.FieldInfo;
import io.github.classgraph.MethodInfoList;
import io.github.classgraph.ScanResult;
import io.github.classgraph.TypeArgument;
import io.github.classgraph.TypeSignature;
import io.github.classgraph.TypeVariableSignature;

public class DddModel {

   private static final boolean DEBUG = false;

   private final Map<RawType<? extends DomainObject>, DomainObjectModel> model;

   private DddModel(Map<RawType<? extends DomainObject>, DomainObjectModel> model) {
      this.model = model;
   }

   public DddModel(List<String> basePackages) {
      this.model = generateModel(basePackages);
   }

   public static DddModel from(DddModel model) {
      return new DddModel(map(() -> new HashMap<>(), model.model));
   }

   public List<DomainObjectModel> domainObjects() {
      return list(model.values());
   }

   /**
    * Normalizes model :
    * <ul>
    *    <li>{@link EntityModel entities} without any references are converted to {@link RootEntityModel root entities}</li>
    * </ul>
    *
    * @return new normalized model
    */
   public DddModel normalizeModel() {
      return new DddModel(model.values().stream().map(dom -> {
         if (dom instanceof EntityModel && !(dom instanceof RootEntityModel) && dom
               .referencedBy()
               .isEmpty()) {
            return RootEntityModel.from((EntityModel) dom);
         } else {
            return dom;
         }
      }).collect(toMap(DomainObjectModel::type, identity())));
   }

   /**
    * Generates all the aggregates from this model, by following references from root entities.
    *
    * @return new aggregates map
    */
   public Map<RootEntityModel, List<DomainObjectModel>> aggregates() {
      Map<RootEntityModel, List<DomainObjectModel>> aggregates = map();
      for (DomainObjectModel domainObjectModel : model.values()) {

         if (domainObjectModel instanceof RootEntityModel) {
            List<DomainObjectModel> aggregate = list(domainObjectModel);

            updateAggregate(domainObjectModel, aggregate);
            aggregates.put((RootEntityModel) domainObjectModel, aggregate);
         }
      }
      return aggregates;
   }

   /**
    * Analyzes the classpath and generate the model.
    */
   private Map<RawType<? extends DomainObject>, DomainObjectModel> generateModel(List<String> basePackages) {
      Map<RawType<? extends DomainObject>, DomainObjectModel> domainObjects = map();
      try (ScanResult scanResult = new ClassGraph()
            .verbose(DEBUG)
            .enableSystemJarsAndModules()
            .enableClassInfo()
            .enableFieldInfo()
            .enableMethodInfo()
            .enableAnnotationInfo()
            .ignoreFieldVisibility()
            .scan()) {
         ClassInfoFilter domainObjectsFilter = domainObjectsFilter(basePackages);
         scanResult
               .getAllClasses()
               .filter(domainObjectsFilter)
               .forEach(ci -> createAndRegisterDomainObjectModel(domainObjects, ci));
         finalizeModel(domainObjects);
      }

      return domainObjects;
   }

   /**
    * Finalizes registered domain objects.
    * Fields, references and referencedBy can only be generated once all domain object models are registered.
    */
   private void finalizeModel(Map<RawType<? extends DomainObject>, DomainObjectModel> domainObjects) {
      new ArrayList<>(domainObjects.values()).forEach(dom -> {
         ClassInfo ci = dom.type().classInfo().orElseThrow(IllegalStateException::new);
         ci.getFieldInfo().forEach(f -> {
            if (isDomainField(ci, f) || f.isEnum()) {
               dom.fields().add(createField(f, domainObjects));
            }
         });

         dom.fields().forEach(field -> {
            field.type().ifPresent(fieldType -> {
               if (fieldType instanceof DomainObjectModel && !fieldType
                     .type()
                     .classInfo()
                     .map(fci -> isId(fci) || isVersion(fci))
                     .orElse(false)) {
                  dom.references().put(field, (DomainObjectModel) fieldType);
               } else if (fieldType instanceof ContainerModel) {
                  ObjectModel containedObjectModel = ((ContainerModel) fieldType).containedType();
                  if (containedObjectModel instanceof DomainObjectModel) {
                     dom.references().put(field, (DomainObjectModel) containedObjectModel);
                  }
               }
            });
         });
      });

      domainObjects.values().forEach(object -> {
         object
               .references()
               .values()
               .forEach(reference -> nullable(domainObjects.get(reference.type())).ifPresent(modelReference -> modelReference
                     .referencedBy()
                     .add(object)));
      });

   }

   private FieldModel createField(FieldInfo f,
                                  Map<RawType<? extends DomainObject>, DomainObjectModel> domainObjects) {
      FieldModel field;

      if (f.isEnum()) {
         field = FieldModel.simple(f.getName());
      } else {
         String fieldName = f.getName();
         TypeSignature typeSignature = f.getTypeSignatureOrTypeDescriptor();

         field = FieldModel.simple(fieldName, createObjectModel(typeSignature, domainObjects));
      }

      return field;
   }

   private ObjectModel createObjectModel(TypeSignature typeSignature,
                                         Map<RawType<? extends DomainObject>, DomainObjectModel> domainObjects) {
      ObjectModel object;

      if (typeSignature instanceof ClassRefTypeSignature) {
         ClassInfo ci = ((ClassRefTypeSignature) typeSignature).getClassInfo();

         // GenericContainerValue : List<TrucId|String> | !DomainObject extends List<TrucId|String> | Stream<TrucId|String>
         if (!hasInterface(ci, DomainObject.class) && hasInterface(ci, Iterable.class, Stream.class)) {
            List<TypeArgument> typeArguments = ((ClassRefTypeSignature) typeSignature).getTypeArguments();

            if (typeArguments.size() > 0) {
               TypeSignature refCollection = typeArguments
                     .get(0)
                     .getTypeSignature();  // FIXME this could be false for some iterable/Stream implementations with multiple generics

               object =
                     new GenericContainerModel(RawType.of(((ClassRefTypeSignature) typeSignature).loadClass(),
                                                          ci),
                                               createObjectModel(refCollection, domainObjects));
            } else {
               object = new SimpleObjectModel(RawType.of(((ClassRefTypeSignature) typeSignature).loadClass(),
                                                         ci));
            }
         } else { // FIXME can be an Iterable/Stream too ?
            object = createAndRegisterDomainObjectModel(domainObjects, ci)
                  .map(ObjectModel.class::cast)
                  .orElseGet(() -> new SimpleObjectModel(RawType.of(((ClassRefTypeSignature) typeSignature).loadClass(),
                                                                    ci)));
         }
      } else if (typeSignature instanceof BaseTypeSignature) {
         object = new SimpleObjectModel(RawType.of(((BaseTypeSignature) typeSignature).getType(), null));
      } else if (typeSignature instanceof ArrayTypeSignature) {
         TypeSignature containedElementType = ((ArrayTypeSignature) typeSignature).getNestedType();

         object = new ArrayModel(createObjectModel(containedElementType, domainObjects));
      } else if (typeSignature instanceof TypeVariableSignature) {
         object =
               new SimpleObjectModel(RawType.of(((TypeVariableSignature) typeSignature).toStringWithTypeBound()));
      } else {
         throw new IllegalStateException("Unknown type signature " + typeSignature);
      }

      return object;
   }

   /**
    * Updates recursively specified aggregate with domain object references.
    * Duplicates are filtered out.
    * If an aggregate references another aggregate root entity, the root entity is filtered out.
    */
   private void updateAggregate(DomainObjectModel domainObject, List<DomainObjectModel> aggregate) {
      List<DomainObjectModel> references = list(domainObject
                                                      .references()
                                                      .values()
                                                      .stream()
                                                      .distinct()
                                                      .filter(r -> !(r instanceof RootEntityModel))
                                                      .filter(r -> !aggregate.contains(r)));
      aggregate.addAll(references);

      for (DomainObjectModel reference : references) {
         updateAggregate(reference, aggregate);
      }
   }

   private ClassInfoFilter domainObjectsFilter(List<String> basePackages) {
      return ci -> basePackagesFilter(basePackages).accept(ci) && domainObjectFilter().accept(ci);
   }

   private ClassInfoFilter basePackagesFilter(List<String> basePackages) {
      return c -> basePackages == null || basePackages.isEmpty() || basePackages
            .stream()
            .anyMatch(bp -> c.getPackageName().equals(bp) || c.getPackageName().startsWith(bp + "."));
   }

   private boolean isDomainField(ClassInfo ci, FieldInfo f) {
      if (!f.isSynthetic() && !f.isTransient() && !(f.isStatic() && !f.isEnum())) {
         MethodInfoList getter = ci.getMethodInfo(f.getName());

         return getter.stream().anyMatch(m -> m.hasAnnotation(Getter.class));
      } else {
         return false;
      }
   }

   /**
    * Creates a new domain object model from introspection information, and registers it in specified model,
    * if not already existing.
    *
    * @implNote Because ids are generated as a side effect of main object creation, it must also be
    *       registered, afterward, to prevent from concurrent modification of model.
    */
   @SuppressWarnings("unchecked")
   private Optional<DomainObjectModel> createAndRegisterDomainObjectModel(Map<RawType<? extends DomainObject>, DomainObjectModel> domainObjects,
                                                                          ClassInfo ci) {
      if (ci == null) {
         return optional();
      }

      Class<? extends DomainObject> objectClass = (Class<? extends DomainObject>) ci.loadClass();

      DomainObjectModel domainObjectModel = domainObjects.get(RawType.of(objectClass, ci));

      if (domainObjectModel != null) {
         return optional(domainObjectModel);
      } else {
         return peek(createDomainObjectModel(domainObjects, ci), dom -> {
            domainObjects.put(dom.type(), dom);
            if (dom instanceof EntityModel) {
               ValueModel id = ((EntityModel) dom).id();
               domainObjects.put(id.type(), id);
            }
         });
      }
   }

   /**
    * Creates a new domain object model from introspection information.
    * Model is specified for searching only, to retrieve already registered object references if already
    * existing.
    */
   @SuppressWarnings("unchecked")
   private Optional<DomainObjectModel> createDomainObjectModel(Map<RawType<? extends DomainObject>, DomainObjectModel> domainObjects,
                                                               ClassInfo ci) {
      if (ci == null) {
         return optional();
      }

      Class<? extends DomainObject> objectClass = (Class<? extends DomainObject>) ci.loadClass();

      return or(nullable(domainObjects.get(RawType.of(objectClass, ci))), () -> {

         DomainObjectModel domainObjectModel = null;
         if (isDomainObject(ci, Value.class)) {
            domainObjectModel = new ValueModel(RawType.of(objectClass, ci));
         } else if (isDomainObject(ci, RootEntity.class)) {
            domainObjectModel = idType(ci)
                  .map(idType -> {
                     ValueModel id = (ValueModel) createDomainObjectModel(domainObjects, idType).orElseThrow(
                           IllegalStateException::new);
                     return new RootEntityModel(RawType.of(objectClass, ci), id);
                  })
                  .orElseThrow(() -> new IllegalStateException("Can't analyze Entity#id() operation for "
                                                               + ci.getName()));
         } else if (isDomainObject(ci, Entity.class)) {
            domainObjectModel = idType(ci)
                  .map(idType -> {
                     ValueModel id = (ValueModel) createDomainObjectModel(domainObjects, idType).orElseThrow(
                           IllegalStateException::new);
                     return new EntityModel(RawType.of(objectClass, ci), id);
                  })
                  .orElseThrow(() -> new IllegalStateException("Can't analyze Entity#id() operation for "
                                                               + ci.getName()));
         }

         return nullable(domainObjectModel);
      });
   }

   /**
    * Identify id class information in specified entity class information.
    */
   private Optional<ClassInfo> idType(ClassInfo ci) {
      return ci.getMethodInfo("id").stream().flatMap(mi -> {
         if (mi.getParameterInfo().length == 0 && mi
               .getTypeDescriptor()
               .getResultType() instanceof ClassRefTypeSignature) {
            ClassInfo idClassInfo =
                  ((ClassRefTypeSignature) mi.getTypeDescriptor().getResultType()).getClassInfo();
            if (isId(idClassInfo)) {
               return stream(idClassInfo);
            }
         }
         return stream();
      }).findFirst();
   }

   /**
    * Returns whether specified class is an "id".
    */
   private boolean isId(ClassInfo ci) {
      Class<?> idClass = ci.loadClass();
      return (Id.class.isAssignableFrom(idClass) && !idClass.equals(Id.class))
             || (BiCompositeId.class.isAssignableFrom(idClass) && !idClass.equals(BiCompositeId.class))
             || (TriCompositeId.class.isAssignableFrom(idClass) && !idClass.equals(TriCompositeId.class));
   }

   /**
    * Returns whether specified class is a "version".
    */
   private boolean isVersion(ClassInfo ci) {
      Class<?> versionClass = ci.loadClass();
      return (Version.class.isAssignableFrom(versionClass) && !versionClass.equals(Version.class));
   }

   private ClassInfoFilter domainObjectFilter() {
      return ci -> ci != null && ci.isPublic() && !ci.isAbstract() && isDomainObject(ci, DomainObject.class);
   }

   private static boolean isDomainObject(ClassInfo ci, Class<? extends DomainObject> domainObjectClass) {
      return hasInterface(ci, domainObjectClass);
   }

   /**
    * Specified {@link ClassInfo} is or implements one of interface classes.
    */
   private static boolean hasInterface(ClassInfo ci, Class<?>... interfaceClasses) {
      return getInterface(ci, interfaceClasses).isPresent();
   }

   private static Optional<ClassInfo> getInterface(ClassInfo ci, Class<?>... interfaceClasses) {
      return streamConcat(stream(ci), ci.getInterfaces().stream())
            .filter(cii -> stream(interfaceClasses)
                  .map(Class::getName)
                  .anyMatch(i -> i.equals(cii.getName())))
            .findFirst();
   }

   private static boolean hasSuperclass(ClassInfo ci, Class<?>... superclasses) {
      return getSuperclasses(ci, superclasses).isPresent();
   }

   private static Optional<ClassInfo> getSuperclasses(ClassInfo ci, Class<?>... superclasses) {
      return streamConcat(stream(ci), ci.getSuperclasses().stream())
            .filter(cii -> stream(superclasses).map(Class::getName).anyMatch(i -> i.equals(cii.getName())))
            .findFirst();
   }

}
