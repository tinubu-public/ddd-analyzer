/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.model;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.findOneOrElseEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tsquare.dddanalyzer.core.DddAnalyze;

/**
 * Entity domain object model.
 */
public class EntityModel extends AbstractDomainObjectModel {

   protected final ValueModel id;

   public EntityModel(RawType<? extends DomainObject> type, ValueModel id) {
      super(type);
      this.id = notNull(id, "id");
   }

   public EntityModel(RawType<?> type,
                      List<FieldModel> fields,
                      ValueModel id,
                      Map<FieldModel, DomainObjectModel> references,
                      List<DomainObjectModel> referencedBy) {
      super(type, fields, references, referencedBy);
      this.id = notNull(id, "id");
   }

   public EntityModel(RawType<?> type, List<FieldModel> fields, ValueModel id) {
      this(type, fields, id, null, null);
   }

   public ValueModel id() {
      return id;
   }

   public FieldModel idField() {
      return findOneOrElseEmpty(fields.stream().filter(f -> f.type()
                                            .map(fieldType -> fieldType.equals(id))
                                            .orElse(false))
                                      .filter(f -> explicitIdFieldName()
                                            .map(idFieldName -> f.name().equals(idFieldName))
                                            .orElse(true))).orElseThrow(() -> new IllegalStateException(String.format(
            "'%s' Entity have multiple matching id fields, use @DddAnalyze(idField=\"<fieldName>\") on class to select the correct id",
            this.type().typeName())));
   }

   private Optional<String> explicitIdFieldName() {
      return type().typeClass().flatMap(domainClass -> {
         DddAnalyze dddAnalyze = domainClass.getAnnotation(DddAnalyze.class);
         return nullable(dddAnalyze).map(DddAnalyze::idField).filter(fn -> !fn.isEmpty());
      });
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      if (!super.equals(o)) return false;
      EntityModel that = (EntityModel) o;
      return Objects.equals(id, that.id);
   }

   @Override
   public int hashCode() {
      return Objects.hash(super.hashCode(), id);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", EntityModel.class.getSimpleName() + "[", "]")
            .add("id=" + id)
            .add("type=" + type)
            //.add("fields=" + fields)
            .toString();
   }

}

//     RootEntityModel[id=ValueModel[type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyEntityId', typeClass=class com.tsquare.dddanalyzer.core.sample.MyEntityId], fields=[]],
//                     fields=[FieldModel[type=ValueModel[type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyEntityId', typeClass=class com.tsquare.dddanalyzer.core.sample.MyEntityId], fields=[]],
//                                        name='id']],
//                     type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyRootEntity', typeClass=class com.tsquare.dddanalyzer.core.sample.MyRootEntity]]]

//     RootEntityModel[id=ValueModel[type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyEntityId', typeClass=class com.tsquare.dddanalyzer.core.sample.MyEntityId], fields=[]],
//                     fields=[FieldModel[type=ValueModel[type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyEntityId', typeClass=class com.tsquare.dddanalyzer.core.sample.MyEntityId], fields=[]],
//                                        name='id'],
//                             FieldModel[type=ValueModel[type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyEntityId', typeClass=class com.tsquare.dddanalyzer.core.sample.MyEntityId], fields=[]],
//                                        name='id'],
//                             FieldModel[type=EntityModel[id=ValueModel[type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyEntityId', typeClass=class com.tsquare.dddanalyzer.core.sample.MyEntityId], fields=[]],
//                                                         type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyEntity', typeClass=class com.tsquare.dddanalyzer.core.sample.MyEntity], fields=[FieldModel[type=ValueModel[type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyEntityId', typeClass=class com.tsquare.dddanalyzer.core.sample.MyEntityId], fields=[]], name='id']]], name='entity']], type=RawType[typeName='com.tsquare.dddanalyzer.core.sample.MyRootEntity', typeClass=class com.tsquare.dddanalyzer.core.sample.MyRootEntity]],