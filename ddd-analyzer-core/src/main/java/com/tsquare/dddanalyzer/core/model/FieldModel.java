/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.model;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * A field model with field name and field type model.
 * For enum constant values, field type is optional.
 */
public class FieldModel {
   private final String name;
   private final ObjectModel type;

   private FieldModel(String name, ObjectModel type) {
      this.name = notBlank(name, "name");
      this.type = type;
   }

   public static FieldModel simple(String name, ObjectModel type) {
      notNull(type, "type");
      return new FieldModel(name, type);
   }

   public static FieldModel simple(String name) {
      return new FieldModel(name, null);
   }

   public String name() {
      return name;
   }

   public Optional<ObjectModel> type() {
      return nullable(type);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      FieldModel that = (FieldModel) o;
      return Objects.equals(name, that.name) && Objects.equals(type, that.type);
   }

   @Override
   public int hashCode() {
      return Objects.hash(name, type);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", FieldModel.class.getSimpleName() + "[", "]")
            .add("type=" + type)
            .add("name='" + name + "'")
            .toString();
   }

}
