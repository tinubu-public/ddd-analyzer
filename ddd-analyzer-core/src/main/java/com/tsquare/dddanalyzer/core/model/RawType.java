/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.model;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notBlank;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

import io.github.classgraph.ClassInfo;

/**
 * Raw type representation.
 * Represents a class to model with associated reflective information.
 *
 * @param <T> class type
 */
public class RawType<T> {

   protected final Class<T> typeClass;
   protected final String typeName;
   protected final ClassInfo classInfo;

   private RawType(Class<T> typeClass, String typeName, ClassInfo classInfo) {
      this.typeClass = typeClass;
      this.typeName = notBlank(typeName, "typeName");
      this.classInfo = classInfo;
   }

   public static <T> RawType<T> of(Class<T> typeClass, ClassInfo classInfo) {
      return new RawType<>(typeClass, typeClass.getName(), classInfo);
   }

   public static <T> RawType<T> of(String typeName) {
      return new RawType<>(null, typeName, null);
   }

   public Optional<Class<T>> typeClass() {
      return nullable(typeClass);
   }

   public String typeName() {
      return typeName;
   }

   public Optional<ClassInfo> classInfo() {
      return nullable(classInfo);
   }

   public boolean isAssignableFrom(Class<?> clazz) {
      return typeClass().map(c -> c.isAssignableFrom(clazz)).orElse(false);
   }

   public boolean isInstanceOf(Class<?> clazz) {
      return typeClass().map(clazz::isAssignableFrom).orElse(false);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      RawType<?> type = (RawType<?>) o;
      return Objects.equals(typeClass, type.typeClass) && Objects.equals(typeName, type.typeName);
   }

   @Override
   public int hashCode() {
      return Objects.hash(typeClass, typeName);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", RawType.class.getSimpleName() + "[", "]")
            .add("typeName='" + typeName + "'")
            .add("typeClass=" + typeClass)
            .toString();
   }

}
