package com.tsquare.dddanalyzer.core;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.assertj.core.api.InstanceOfAssertFactories;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.versions.LongVersion;
import com.tsquare.dddanalyzer.core.model.AbstractDomainObjectModel;
import com.tsquare.dddanalyzer.core.model.DddModel;
import com.tsquare.dddanalyzer.core.model.DomainObjectModel;
import com.tsquare.dddanalyzer.core.model.EntityModel;
import com.tsquare.dddanalyzer.core.model.FieldModel;
import com.tsquare.dddanalyzer.core.model.GenericContainerModel;
import com.tsquare.dddanalyzer.core.model.RawType;
import com.tsquare.dddanalyzer.core.model.RootEntityModel;
import com.tsquare.dddanalyzer.core.model.SimpleObjectModel;
import com.tsquare.dddanalyzer.core.model.ValueModel;
import com.tsquare.dddanalyzer.core.sample.MyEntity;
import com.tsquare.dddanalyzer.core.sample.MyEntityId;
import com.tsquare.dddanalyzer.core.sample.MyEnum;
import com.tsquare.dddanalyzer.core.sample.MyGenPojo;
import com.tsquare.dddanalyzer.core.sample.MyGenValue;
import com.tsquare.dddanalyzer.core.sample.MyPojo;
import com.tsquare.dddanalyzer.core.sample.MyRootEntity;
import com.tsquare.dddanalyzer.core.sample.MyValue;
import com.tsquare.dddanalyzer.core.sample.MyVersionedEntity;

public class DddModelTest {

   @Test
   public void testModelWhenNominal() {
      DddModel model = new DddModel(list("com.tsquare.dddanalyzer.core.sample")).normalizeModel();

      assertThat(model.domainObjects())
            //.usingComparatorForType(domainObjectModelComparator(), AbstractDomainObjectModel.class)
            .containsExactlyInAnyOrder(ModelFixtures.myValueModel,
                                       ModelFixtures.myGenValue,
                                       ModelFixtures.myEntityIdModel,
                                       ModelFixtures.myEntityModel,
                                       ModelFixtures.myEnumModel,
                                       ModelFixtures.myRootEntityModel,
                                       ModelFixtures.versionModel,
                                       ModelFixtures.myVersionedEntityModel);
   }

   private static Comparator<AbstractDomainObjectModel> domainObjectModelComparator() {
      return (o1, o2) -> o1.fullyEquals(o2) ? 0 : 1;
   }

   @Test
   public void testAggregates() {
      Map<RootEntityModel, List<DomainObjectModel>> aggregates =
            new DddModel(list("com.tsquare.dddanalyzer.core.sample")).normalizeModel().aggregates();

      assertThat(aggregates).containsOnlyKeys(ModelFixtures.myRootEntityModel,
                                              ModelFixtures.myVersionedEntityModel);
      assertThat(aggregates)
            .extractingByKey(ModelFixtures.myRootEntityModel,
                             as(InstanceOfAssertFactories.list(DomainObjectModel.class)))
            .containsExactlyInAnyOrder(ModelFixtures.myRootEntityModel,
                                       ModelFixtures.myEntityModel,
                                       ModelFixtures.myValueModel,
                                       ModelFixtures.myGenValue,
                                       ModelFixtures.myEnumModel);
      assertThat(aggregates)
            .extractingByKey(ModelFixtures.myVersionedEntityModel,
                             as(InstanceOfAssertFactories.list(DomainObjectModel.class)))
            .containsExactlyInAnyOrder(ModelFixtures.myVersionedEntityModel,
                                       ModelFixtures.myValueModel,
                                       ModelFixtures.myEnumModel,
                                       ModelFixtures.myGenValue);
   }

   public static class ModelFixtures {
      private final static SimpleObjectModel tModel = new SimpleObjectModel(RawType.of("T"));
      private final static SimpleObjectModel intModel = new SimpleObjectModel(RawType.of(int.class, null));
      private final static SimpleObjectModel myPojoModel =
            new SimpleObjectModel(RawType.of(MyPojo.class, null));
      private final static SimpleObjectModel myGenPojoModel =
            new SimpleObjectModel(RawType.of(MyGenPojo.class, null));
      private final static ValueModel myEnumModel = new ValueModel(RawType.of(MyEnum.class, null),
                                                                   list(FieldModel.simple("VALUE1"),
                                                                        FieldModel.simple("VALUE2")));
      private final static ValueModel myGenValueModel =
            new ValueModel(RawType.of(MyGenValue.class, null), list(FieldModel.simple("value", tModel)));
      private final static ValueModel myValueModel = new ValueModel(RawType.of(MyValue.class, null),
                                                                    list(FieldModel.simple("intValue",
                                                                                           intModel),
                                                                         FieldModel.simple("enumValue",
                                                                                           myEnumModel),
                                                                         FieldModel.simple("pojoValue",
                                                                                           myPojoModel),
                                                                         FieldModel.simple("genPojoValue",
                                                                                           myGenPojoModel),
                                                                         FieldModel.simple("genValue",
                                                                                           myGenValueModel),
                                                                         FieldModel.simple("pojoValues",
                                                                                           new GenericContainerModel(
                                                                                                 RawType.of(
                                                                                                       List.class,
                                                                                                       null),
                                                                                                 myPojoModel))/*,
                                                                         FieldModel.simple(
                                                                               "autoReferenceValue",
                                                                               myValueModel)*/));
      private final static ValueModel myGenValue =
            new ValueModel(RawType.of(MyGenValue.class, null), list(FieldModel.simple("value", tModel)));
      private final static ValueModel myEntityIdModel = new ValueModel(RawType.of(MyEntityId.class, null));
      private final static EntityModel myEntityModel = new EntityModel(RawType.of(MyEntity.class, null),
                                                                       list(FieldModel.simple("myId",
                                                                                              myEntityIdModel),
                                                                            FieldModel.simple("value",
                                                                                              myValueModel)),
                                                                       myEntityIdModel);
      private final static RootEntityModel myRootEntityModel =
            new RootEntityModel(RawType.of(MyRootEntity.class, null),
                                list(FieldModel.simple("id", myEntityIdModel),
                                     FieldModel.simple("entity", myEntityModel)),
                                myEntityIdModel);
      private final static ValueModel versionModel = new ValueModel(RawType.of(LongVersion.class, null));
      private final static RootEntityModel myVersionedEntityModel =
            new RootEntityModel(RawType.of(MyVersionedEntity.class, null),
                                list(FieldModel.simple("myId", myEntityIdModel),
                                     FieldModel.simple("version", versionModel),
                                     FieldModel.simple("value", myValueModel)),
                                myEntityIdModel);
   }
}
