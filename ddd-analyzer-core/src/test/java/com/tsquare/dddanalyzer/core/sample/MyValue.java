/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.sample;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tsquare.dddanalyzer.core.sample.MyEnum.VALUE1;

import java.util.List;

import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.lang.beans.Getter;

public class MyValue implements Value {

   private final int intValue;
   private final MyEnum enumValue;
   private final MyPojo pojoValue;
   private final MyGenPojo<Integer> genPojoValue;
   private final MyGenValue<Integer> genValue;
   private final List<MyPojo> pojoValues;
   private final MyValue autoReferenceValue;

   public MyValue() {
      this.intValue = 0;
      this.enumValue = VALUE1;
      this.pojoValue = new MyPojo();
      this.genPojoValue = new MyGenPojo<>();
      this.genValue = new MyGenValue<>(0);
      this.pojoValues = list(new MyPojo(), new MyPojo());
      this.autoReferenceValue = this;
   }

   @Getter
   public int intValue() {
      return intValue;
   }

   @Getter
   public MyEnum enumValue() {
      return enumValue;
   }

   @Getter
   public MyPojo pojoValue() {
      return pojoValue;
   }

   @Getter
   public MyGenPojo<Integer> genPojoValue() {
      return genPojoValue;
   }

   @Getter
   public MyGenValue<Integer> genValue() {
      return genValue;
   }

   @Getter
   public List<MyPojo> pojoValues() {
      return pojoValues;
   }

   @Getter
   public MyValue autoReferenceValue() {
      return autoReferenceValue;
   }
}
