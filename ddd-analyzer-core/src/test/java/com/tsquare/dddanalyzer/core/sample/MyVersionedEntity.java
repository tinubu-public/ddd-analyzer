/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tsquare.dddanalyzer.core.sample;

import com.tinubu.commons.ddd2.domain.type.VersionedEntity;
import com.tinubu.commons.ddd2.domain.versions.LongVersion;
import com.tinubu.commons.lang.beans.Getter;

public class MyVersionedEntity implements VersionedEntity<MyEntityId, LongVersion> {

   private final MyEntityId myId;
   private final LongVersion version;
   private final MyValue value;

   public MyVersionedEntity(MyEntityId myId, MyValue value) {
      this.myId = myId;
      this.version = LongVersion.initialVersion();
      this.value = value;
   }

   @Getter
   public MyEntityId myId() {
      return myId;
   }

   @Getter
   public MyValue value() {
      return value;
   }

   @Override
   public MyEntityId id() {
      return myId;
   }

   @Getter
   @Override
   public LongVersion version() {
      return version;
   }
}
