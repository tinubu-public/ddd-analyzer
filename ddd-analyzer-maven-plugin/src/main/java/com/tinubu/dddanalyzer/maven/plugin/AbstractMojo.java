/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.dddanalyzer.maven.plugin;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.maven.plugins.annotations.Parameter;

public abstract class AbstractMojo extends org.apache.maven.plugin.AbstractMojo {

   @Parameter(property = "project.compileClasspathElements", required = true, readonly = true)
   protected List<String> moduleCompileClasspath;

   protected ClassLoader classLoader() {
      return Thread.currentThread().getContextClassLoader();
   }

   protected void loadModuleClasspath() {
      getLog().debug("Use classpath : " + String.join(",", moduleCompileClasspath));

      Set<URI> uris = new HashSet<>();
      for (String element : moduleCompileClasspath) {
         uris.add(new File(element).toURI());
      }

      ClassLoader contextClassLoader =
            URLClassLoader.newInstance(uris.stream().map(this::toURL).toArray(URL[]::new), classLoader());

      Thread.currentThread().setContextClassLoader(contextClassLoader);
   }

   protected URL toURL(URI uri) {
      try {
         return uri.toURL();
      } catch (MalformedURLException e) {
         throw new IllegalStateException(e);
      }
   }
}
