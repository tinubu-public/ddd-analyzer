/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.dddanalyzer.maven.plugin.analyze;

import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import com.tinubu.dddanalyzer.maven.plugin.AbstractMojo;
import com.tsquare.dddanalyzer.core.analyzer.DddAnalyzer;
import com.tsquare.dddanalyzer.core.model.DddModel;

/**
 * Analyzes DDD domain and check rules.
 */
@Mojo(name = "analyze", defaultPhase = LifecyclePhase.COMPILE,
      requiresDependencyResolution = ResolutionScope.COMPILE)
public class AnalyzeMojo extends AbstractMojo {

   /** Skip mojo if true. */
   @Parameter(property = "ddd-analyzer.skipAnalyze", required = true, defaultValue = "false")
   private boolean skip;

   /**
    * Optional packages to analyze. Packages must be fully qualified.
    */
   @Parameter(property = "ddd-analyzer.packages")
   private List<String> packages;

   /** Fails on warnings. */
   @Parameter(property = "ddd-analyzer.analyzeFailOnWarnings", required = true, defaultValue = "false")
   private boolean analyzeFailOnWarnings;

   /** Fails on error. */
   @Parameter(property = "ddd-analyzer.analyzeFailOnErrors", required = true, defaultValue = "true")
   private boolean analyzeFailOnErrors;

   @Override
   public void execute() throws MojoExecutionException {
      if (!skip) {
         loadModuleClasspath();

         DddModel model = new DddModel(packages);

         DddAnalyzer analyze = new DddAnalyzer(model, this::warningCallback, this::errorCallback);

         String analyzeMessage = String.format("DDD analyze found %d warning(s), %d error(s)",
                                               analyze.warningCount(),
                                               analyze.errorCount());

         if ((analyze.warningCount() > 0 && analyzeFailOnWarnings) || (analyze.errorCount() > 0
                                                                       && analyzeFailOnErrors)) {
            getLog().error(analyzeMessage);
            throw new MojoExecutionException(analyzeMessage);
         } else {
            getLog().info(analyzeMessage);
         }
      }
   }

   private void warningCallback(String message) {
      if (analyzeFailOnWarnings) {
         getLog().error(message);
      } else {
         getLog().warn(message);
      }
   }

   private void errorCallback(String message) {
      if (analyzeFailOnErrors) {
         getLog().error(message);
      } else {
         getLog().warn(message);
      }

   }
}
